# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# Change this when you update the ebuild
GIT_COMMIT="8b0a9deb922b5a1fd85ab3c9a2b135dab5348f8d"
EGO_PN="github.com/influxdata/${PN}"
EGO_VENDOR=(
       "github.com/BurntSushi/toml v0.3.1"
       "github.com/Masterminds/semver v1.4.2"
       "github.com/NYTimes/gziphandler 6710af535839"
       "github.com/Sirupsen/logrus 3ec0642a7fb6"
       "github.com/alecthomas/kingpin v2.2.6"
       "github.com/alecthomas/template a0175ee3bccc"
       "github.com/alecthomas/units 2efee857e7cf"
       "github.com/apache/arrow cf047fc67698"
       "github.com/apex/log v1.1.0"
       "github.com/aws/aws-sdk-go v1.16.18"
       "github.com/blakesmith/ar 8bd4349a67f2"
       "github.com/boltdb/bolt 5cc10bbbc5c1"
       "github.com/bouk/httprouter ee8b3818a7f5"
       "github.com/caarlos0/ctrlc v1.0.0"
       "github.com/campoy/unique 88950e537e7e"
       "github.com/cespare/xxhash v1.1.0"
       "github.com/dgrijalva/jwt-go 24c63f56522a"
       "github.com/dustin/go-humanize bb3d318650d4"
       "github.com/elazarl/go-bindata-assetfs 9a6736ed45b4"
       "github.com/fatih/color v1.7.0"
       "github.com/go-sql-driver/mysql v1.4.0"
       "github.com/gogo/protobuf 49944b4a4b08"
       "github.com/golang/protobuf v1.2.0"
       "github.com/google/go-cmp v0.2.0"
       "github.com/google/go-github v17.0.0"
       "github.com/google/go-querystring v1.0.0"
       "github.com/goreleaser/goreleaser v0.97.0"
       "github.com/goreleaser/nfpm v0.9.7"
       "github.com/imdario/mergo v0.3.6"
       "github.com/influxdata/flux v0.25.0"
       "github.com/influxdata/influxdb v1.1.5"
       "github.com/influxdata/kapacitor v1.5.0"
       "github.com/influxdata/line-protocol 32c6aa80de5e"
       "github.com/influxdata/tdigest bf2b5ad3c0a9"
       "github.com/influxdata/usage-client 6d3895376368"
       "github.com/jessevdk/go-flags v1.4.0"
       "github.com/jmespath/go-jmespath c2b33e8439af"
       "github.com/kamilsk/retry 495c1d672c93"
       "github.com/kevinburke/go-bindata 46eb4c183bfc"
       "github.com/lib/pq v1.0.0"
       "github.com/mattn/go-colorable v0.0.9"
       "github.com/mattn/go-isatty v0.0.4"
       "github.com/mattn/go-zglob v0.0.1"
       "github.com/microcosm-cc/bluemonday v1.0.1"
       "github.com/mitchellh/go-homedir v1.0.0"
       "github.com/opentracing/opentracing-go v1.0.2"
       "github.com/pkg/errors v0.8.0"
       "github.com/satori/go.uuid v1.2.0"
       "github.com/segmentio/kafka-go v0.2.0"
       "github.com/sergi/go-diff v1.0.0"
       "github.com/tylerb/graceful v1.2.15"
       "go.uber.org/atomic v1.3.2 github.com/uber-go/atomic"
       "go.uber.org/multierr v1.1.0 github.com/uber-go/multierr"
       "go.uber.org/zap v1.9.1 github.com/uber-go/zap"
       "golang.org/x/net 9b4f9f5ad519 github.com/golang/net"
       "golang.org/x/oauth2 9dcd33a902f4 github.com/golang/oauth2"
       "golang.org/x/sync 37e7f081c4d4 github.com/golang/sync"
       "golang.org/x/sys a5c9d58dba9a github.com/golang/sys"
       "golang.org/x/tools 36f37f8f5c81 github.com/golang/tools"
       "google.golang.org/api bc20c61134e1 github.com/googleapis/google-api-go-client"
       "google.golang.org/appengine v1.2.0 github.com/golang/appengine"
       "gopkg.in/yaml.v2 v2.2.2 github.com/go-yaml/yaml"
       "honnef.co/go/tools c2f93a96b099 github.com/dominikh/go-tools"
)

inherit golang-vcs-snapshot systemd user

DESCRIPTION="Open source monitoring and visualization UI for the TICK stack"
HOMEPAGE="https://www.influxdata.com"
ARCHIVE_URI="https://${EGO_PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
SRC_URI="${ARCHIVE_URI} ${EGO_VENDOR_URI}"
RESTRICT="mirror"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE="debug pie static"

DEPEND="
	>=net-libs/nodejs-8.12.0
	sys-apps/yarn
"

DOCS=( CHANGELOG.md )
QA_PRESTRIPPED="usr/bin/.*"

G="${WORKDIR}/${P}"
S="${G}/src/${EGO_PN}"

pkg_pretend() {
	# shellcheck disable=SC2086
	if has network-sandbox ${FEATURES} && [[ "${MERGE_TYPE}" != binary ]]; then
		ewarn
		ewarn "${CATEGORY}/${PN} requires 'network-sandbox' to be disabled in FEATURES"
		ewarn
		die "[network-sandbox] is enabled in FEATURES"
	fi
}

pkg_setup() {
	enewgroup chronograf
	enewuser chronograf -1 -1 /var/lib/chronograf chronograf
}

src_prepare() {
	# Remove git calls, as the tarball isn't a proper git repository
	sed -i "/COMMIT ?=/d" Makefile || die
	sed -i "s:GIT_SHA=\$(git rev-parse HEAD):GIT_SHA=${GIT_COMMIT}:" \
		ui/package.json || die

	default
}

src_compile() {
	export GOPATH="${G}"
	export GOBIN="${S}/bin"
	export CGO_CFLAGS="${CFLAGS}"
	export CGO_LDFLAGS="${LDFLAGS}"
	(use static && ! use pie) && export CGO_ENABLED=0
	(use static && use pie) && CGO_LDFLAGS+=" -static"
	local PATH="${GOBIN}:$PATH"

	local myldflags=(
		"$(usex !debug '-s -w' '')"
		-X "main.version=${PV}"
		-X "main.commit=${GIT_COMMIT:0:8}"
	)

	local mygoargs=(
		-v -work -x
		-buildmode "$(usex pie pie exe)"
		-asmflags "all=-trimpath=${S}"
		-gcflags "all=-trimpath=${S}"
		-ldflags "${myldflags[*]}"
		-tags "$(usex static 'netgo' '')"
		-installsuffix "$(usex static 'netgo' '')"
	)

	# Build go-bindata locally
	go install ./vendor/github.com/kevinburke/go-bindata/go-bindata || die

	emake .jsdep
	touch .godep || die
	emake .jssrc
	emake .bindata

	go install "${mygoargs[@]}" ./cmd/{chronograf,chronoctl} || die
}

src_install() {
	dobin bin/{chronoctl,chronograf}
	use debug && dostrip -x /usr/bin/{chronoctl,chronograf}
	einstalldocs

	newinitd "${FILESDIR}/${PN}.initd" "${PN}"
	newconfd "${FILESDIR}/${PN}.confd" "${PN}"
	systemd_dounit "etc/scripts/${PN}.service"

	insinto /usr/share/chronograf/canned
	doins canned/*.json

	insinto /usr/share/chronograf/protoboards
	doins protoboards/*.json
	dodir /usr/share/chronograf/resources

	insinto /etc/logrotate.d
	newins etc/scripts/logrotate chronograf

	diropts -o chronograf -g chronograf -m 0750
	keepdir /var/log/chronograf
}
