# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# Change this when you update the ebuild
GIT_COMMIT="01c8dd416270f424ab0c40f9291e269ac6921964"
EGO_PN="github.com/influxdata/${PN}"
# Note: Keep EGO_VENDOR in sync with Gopkg.lock
EGO_VENDOR=(
	"collectd.org 2ce144541b89 github.com/collectd/go-collectd"
	"github.com/BurntSushi/toml a368813c5e64"
    "github.com/Masterminds/semver c7af12943936"
	"github.com/alecthomas/kingpin 947dcec5ba9c"
	"github.com/alecthomas/template a0175ee3bccc"
	"github.com/alecthomas/units 2efee857e7cf"
	"github.com/apache/arrow f5df77359953"
	"github.com/apex/log 941dea75d3eb"
	"github.com/aws/aws-sdk-go dd947f47decb"
	"github.com/beorn7/perks 3a771d992973"
	"github.com/blakesmith/ar 8bd4349a67f2"
	"github.com/bmizerany/pat 6226ea591a40"
	"github.com/boltdb/bolt 2f1ce7a837dc"
	"github.com/c-bata/go-prompt e99fbc797b79"
	"github.com/caarlos0/ctrlc 70dc48d5d792"
	"github.com/campoy/unique 88950e537e7e"
	"github.com/cespare/xxhash 5c37fe373534"
	"github.com/davecgh/go-spew 346938d642f2"
	"github.com/dgrijalva/jwt-go 06ea1031745c"
	"github.com/dgryski/go-bitstream 3522498ce2c8"
	"github.com/fatih/color 570b54cabe6b"
	"github.com/glycerine/go-unsnap-stream 9f0cb55181dd"
	"github.com/go-ini/ini 7b294651033c"
	"github.com/go-sql-driver/mysql 72cd26f257d4"
	"github.com/gogo/protobuf 636bf0302bc9"
    "github.com/golang/protobuf b4deda0973fb"
	"github.com/golang/snappy d9eb7a3d35ec"
	"github.com/google/go-cmp 3af367b6b30c"
	"github.com/google/go-github dd29b543e14c"
	"github.com/google/go-querystring 44c6ddd0a234"
	"github.com/goreleaser/goreleaser f99940ff5397"
	"github.com/goreleaser/nfpm de75d6799013"
	"github.com/imdario/mergo 9f23e2d6bd2a"
	"github.com/influxdata/flux bcfc535fb744"
	"github.com/influxdata/influxql 1cbfca8e56b6"
	"github.com/influxdata/line-protocol a3afd890113f"
	"github.com/influxdata/roaring fc520f41fab6"
	"github.com/influxdata/tdigest bf2b5ad3c0a9"
	"github.com/influxdata/usage-client 6d3895376368"
	"github.com/jmespath/go-jmespath 0b12d6b5"
	"github.com/jsternberg/zap-logfmt ac4bd917e18a"
	"github.com/jwilder/encoding b4e1701a28ef"
	"github.com/kisielk/gotool 80517062f582"
	"github.com/klauspost/compress b939724e787a"
	"github.com/klauspost/cpuid ae7887de9fa5"
	"github.com/klauspost/crc32 cb6bfca970f6"
	"github.com/klauspost/pgzip 0bf5dcad4ada"
	"github.com/lib/pq 4ded0e9383f7"
	"github.com/mattn/go-colorable 167de6bfdfba"
	"github.com/mattn/go-isatty 6ca4dbf54d38"
	"github.com/mattn/go-runewidth 9e777a8366cc"
	"github.com/mattn/go-tty 13ff1204f104"
	"github.com/mattn/go-zglob 2ea3427bfa53"
	"github.com/matttproud/golang_protobuf_extensions c12348ce28de"
	"github.com/mitchellh/go-homedir ae18d6b8b320"
	"github.com/mschoch/smat 90eadee771ae"
	"github.com/opentracing/opentracing-go bd9c31933947"
	"github.com/paulbellamy/ratecounter 524851a93235"
	"github.com/peterh/liner 8c1271fcf47f"
	"github.com/philhofer/fwd bb6d471dc95d"
	"github.com/pkg/errors 645ef00459ed"
	"github.com/pkg/term bffc007b7fd5"
	"github.com/prometheus/client_golang 661e31bf844d"
	"github.com/prometheus/client_model 5c3871d89910"
	"github.com/prometheus/common 7600349dcfe1"
	"github.com/prometheus/procfs ae68e2d4c00f"
	"github.com/retailnext/hllpp 101a6d2f8b52"
	"github.com/satori/go.uuid f58768cc1a7a"
	"github.com/segmentio/kafka-go 0b3aacc52781"
	"github.com/spf13/cast 8c9545af88b1"
	"github.com/tinylib/msgp b2b6a672cf1e"
	"github.com/willf/bitset d860f346b894"
	"github.com/xlab/treeprint d6fb6747feb6"
	"go.uber.org/atomic v1.3.2 github.com/uber-go/atomic"
	"go.uber.org/multierr v1.1.0 github.com/uber-go/multierr"
	"go.uber.org/zap v1.9.0 github.com/uber-go/zap"
	"golang.org/x/crypto a2144134853f github.com/golang/crypto"
	"golang.org/x/net a680a1efc54d github.com/golang/net"
	"golang.org/x/oauth2 c57b0facaced github.com/golang/oauth2"
	"golang.org/x/sync 1d60e4601c6f github.com/golang/sync"
	"golang.org/x/sys ac767d655b30 github.com/golang/sys"
	"golang.org/x/text f21a4dfb5e38 github.com/golang/text"
	"golang.org/x/time fbb02b2291d2 github.com/golang/time"
	"golang.org/x/tools 45ff765b4815 github.com/golang/tools"
	"google.golang.org/appengine v1.2.0 github.com/golang/appengine"
	"google.golang.org/genproto fedd2861243f github.com/google/go-genproto"
	"google.golang.org/grpc v1.13.0 github.com/grpc/grpc-go"
	"gopkg.in/yaml.v2 5420a8b6744d github.com/go-yaml/yaml"
	"honnef.co/go/tools 2017.2.2 github.com/dominikh/go-tools"
)

inherit golang-vcs-snapshot systemd user

MY_PV="${PV/_/}"
DESCRIPTION="Scalable datastore for metrics, events, and real-time analytics"
HOMEPAGE="https://influxdata.com"
ARCHIVE_URI="https://${EGO_PN}/archive/v${MY_PV}.tar.gz -> ${P}.tar.gz"
SRC_URI="${ARCHIVE_URI} ${EGO_VENDOR_URI}"
RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug man pie static"

DEPEND="man? ( app-text/asciidoc app-text/xmlto )"

QA_PRESTRIPPED="usr/bin/.*"

G="${WORKDIR}/${P}"
S="${G}/src/${EGO_PN}"

pkg_setup() {
	enewgroup influxdb
	enewuser influxdb -1 -1 /var/lib/influxdb influxdb
}

src_prepare() {
	# By default InfluxDB sends anonymous statistics to
	# usage.influxdata.com, which is a no-no!
	sed -i "s:# reporting.*:reporting-disabled = true:" \
		etc/config.sample.toml || die

	default
}

src_compile() {
	export GOPATH="${G}"
	export GOBIN="${S}"
	export CGO_CFLAGS="${CFLAGS}"
	export CGO_LDFLAGS="${LDFLAGS}"
	(use static && ! use pie) && export CGO_ENABLED=0
	(use static && use pie) && CGO_LDFLAGS+=" -static"

	local myldflags=(
		"$(usex !debug '-s -w' '')"
		-X "main.branch=non-git"
		-X "main.commit=${GIT_COMMIT:0:7}"
		-X "main.version=${MY_PV}"
	)

	local mygoargs=(
		-v -work -x
		-buildmode "$(usex pie pie exe)"
		-asmflags "all=-trimpath=${S}"
		-gcflags "all=-trimpath=${S}"
		-ldflags "${myldflags[*]}"
		-tags "$(usex static 'netgo' '')"
		-installsuffix "$(usex static 'netgo' '')"
	)

	go install "${mygoargs[@]}" ./cmd/influx{,d,_inspect,_stress,_tools,_tsm} || die

	use man && emake -C man
}

src_test() {
	go test -v -timeout 10s ./cmd/influxd/run || die
}

src_install() {
	dobin influx{,d,_inspect,_stress,_tools,_tsm}
	use debug && dostrip -x /usr/bin/influx{,d,_inspect,_stress,_tools,_tsm}

	newinitd "${FILESDIR}/${PN}.initd" "${PN}"
	newconfd "${FILESDIR}/${PN}.confd" "${PN}"
	systemd_install_serviced "${FILESDIR}/${PN}.service.conf"
	systemd_dounit "scripts/${PN}.service"

	insinto /etc/influxdb
	newins etc/config.sample.toml influxdb.conf.example

	use man && doman man/*.1

	diropts -o influxdb -g influxdb -m 0750
	keepdir /var/log/influxdb
}

pkg_postinst() {
	if [[ ! -e "${EROOT}/etc/influxdb/influxdb.conf" ]]; then
		elog "No influxdb.conf found, copying the example over"
		cp "${EROOT}"/etc/influxdb/influxdb.conf{.example,} || die
	else
		elog "influxdb.conf found, please check example file for possible changes"
	fi
}
